Parses a `torrc` file and produces a health check on the included bridges.

Currently only supports vanilla bridges with fingerprints specified in `torrc` like this:
```
Bridge 0.0.0.0:0000 0E158AC201BD0F3FA3C462F64844CBFFC7297A76
Bridge 1.1.1.1:1111 15D64D5D44E20169585C7368580C0D33A872AD45
```