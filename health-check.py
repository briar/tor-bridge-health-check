#!/usr/bin/env python3
import hashlib
import json
import os
import re
import sys
from binascii import a2b_hex

import requests

URL_DETAILS = "https://onionoo.torproject.org/details?"

HEALTH_FLAGS = ["Valid", "Running", "Stable", "Fast"]
REGEX = re.compile(r'Bridge (obfs4 )?[0-9.:]+ ([0-9A-F]{40}).*')


def main():
    if len(sys.argv) != 2:
        fail("Usage: %s path/to/torrc" % sys.argv[0])
    torrc = sys.argv[1]

    if not os.path.isfile(torrc):
        fail("File %s not found" % torrc)

    fingerprints = get_fingerprints(torrc)
    if len(fingerprints) == 0:
        fail("No bridges in the required format found in %s" % torrc)

    issue_found = False
    for fingerprint in fingerprints:
        ok = print_health_check(fingerprint)
        issue_found = issue_found or not ok
        print()

    sys.exit(1 if issue_found else 0)


def get_fingerprints(torrc):
    fingerprints = []
    with open(torrc, 'r') as f:
        for line in f.readlines():
            if not line.startswith("Bridge "):
                continue
            match = REGEX.match(line)
            if not match or not match.group(2):
                continue
            fingerprints.append(match.group(2))
    return fingerprints


def print_health_check(fingerprint):
    print("Checking bridge %s..." % fingerprint)
    # Search Onionoo for bridge
    hashed_fingerprint = get_tor_sha1(fingerprint)
    url = URL_DETAILS + "lookup=%s" % hashed_fingerprint
    response = requests.get(url)
    bridges = json.loads(response.content)['bridges']
    if len(bridges) != 1:
        print("Warning: Bridge NOT found in Onionoo %s" % url)
        return False
    bridge = bridges[0]
    has_issue = False
    # Check if bridge is running
    if bridge['running'] is not True:
        has_issue = True
        print("Warning: Bridge is not running!")
    # Check bridge flags
    missing_flags = []
    for flag in HEALTH_FLAGS:
        if flag not in bridge['flags']:
            missing_flags.append(flag)
    if len(missing_flags) > 0:
        has_issue = True
        print("Warning: Bridge is missing flag(s): %s" % ', '.join(missing_flags))
    # Check recommended version
    if 'recommended_version' not in bridge:
        has_issue = True
        print("Warning: We don't know if bridge is running a recommended version")
    elif bridge['recommended_version'] is not True:
        has_issue = True
        version = bridge['version'] if 'version' in bridge else 'unknown'
        print("Warning: Bridge is not running a recommended version, but %s" % version)
    # Return True if no issues were want, False otherwise
    if has_issue:
        return False
    print("OK")
    return True


def get_tor_sha1(to_hash):
    sha1 = hashlib.sha1()
    sha1.update(a2b_hex(to_hash))
    return sha1.hexdigest()


def fail(msg=""):
    sys.stderr.write("Error: %s\n" % msg)
    sys.exit(1)


if __name__ == "__main__":
    main()
